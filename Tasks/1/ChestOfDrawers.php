<?php
include "Furniture.php";

class ChestOfDrawers extends Furniture{
    private $material;
    private $size;
	private $numberOfShelves;
	private $colorOfPens;
	private $score;
    
    function __construct($name,$price,$score){
        $this->name = $name;
        $this->price = $price;
        $this->score = $score;
    }
    
    public function getMaterial(){
        return $this->material;
    }
    public function setMaterial($material){
         $this->material = $material;
    }
    
    public function getSize(){
        return $this->size;
    }
    public function setSize($size){
         $this->size = $size;
    }
	
	public function getNumberOfShelves(){
        return $this->numberOfShelves;
    }
    public function setNumberOfShelves($numberOfShelves){
         $this->numberOfShelves = $numberOfShelves;
    }
    
    public function getColorOfPens(){
        return $this->manufacturer;
    }
    public function setColorOfPens($colorOfPens){
         $this->colorOfPens = $colorOfPens;
    
    public function showInfo(){
        echo "Комод ".$this->name." стоит ".$this->price.", производитель ".$this->manufacturer.", цвет ".$this->color."<hr>";
        echo "Размер ".$this->size.", материал ".$this->material.", Количество полок ".$this->numberOfShelves.", цвет ручек ".$this->colorOfPens."<hr>";
    }
	
	public function removeFromStock($number){
		echo "<hr><h2>Списание со склада</h2>";
		if (($this->score - $number) < 0) {
			echo "<b>Недостаточное количество товара на складе для списания: $number шт.!</b><br>";
		} else {
			$this->score -= $number;
			echo "<b>Списание товара $this->title в количестве $number шт. выполнено успешно!</b><br>";
		}
		echo "<b>Остаток на складе:</b> $this->score шт.<br>";
	}
}