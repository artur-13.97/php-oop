<?php

class Furniture{
    private $name;
    private $price;
	private $color;
	private $manufacturer;
    
    function __construct($name,$price){
        $this->name = $name;
        $this->price = $price;
    }
    
    public function getName(){
        return $this->name;
    }
    public function setName($name){
         $this->name = $name;
    }
    
    public function getPrice(){
        return $this->price;
    }
    public function setPrice($price){
         $this->price = $price;
    }
	
	public function getColor(){
        return $this->color;
    }
    public function setColor($color){
         $this->color = $color;
    }
    
    public function getManufacturer(){
        return $this->manufacturer;
    }
    public function setManufacturer($manufacturer){
         $this->manufacturer = $manufacturer;
    
    function showInfo(){
        echo "Мебель ".$this->name." стоит ".$this->price.", производитель ".$this->manufacturer.", цвет ".$this->color."<hr>";
    }
}