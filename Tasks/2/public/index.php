<?php

include "../services/Autoloader.php";

spl_autoload_register([new \app\services\Autoloader(), 'loadClass']);

//public function __construct($article, $category, $title, $description, $size, $weight, $price, $guarantee, $country, $count)

/*Товар на вес*/
$productWeight = new \app\models\Product_Weight(942156, "Диван", "Диван кожаный",
    "Солидная модель классического дизайна, которая отлично подойдет и для стильной гостиной, и для простороного рабочего кабинета.",
    null, null, 114.5, null, "США", "10");

echo "Покупка 1 (3 шт): {$productReal->getPrice(3)} руб.<br>";
echo "Покупка 2 (5 шт): {$productReal->getPrice(5)} руб.<br>";
echo "Доход с продаж: {$productWeight->getTotal()} руб.<br>";
var_dump($productWeight);


/*Штучный товар*/
$productReal = new \app\models\Product_Real(137538198, "Диван", "Диван тканевый",
    "Яркий пример удачного сочетания элегантного стиля и функциональности.",
    null, null, 969, null, "Россия", "7");

echo "Покупка 1 (1 шт): {$productReal->getPrice(1)} руб.<br>";
echo "Покупка 2 (2 шт): {$productReal->getPrice(2)} руб.<br>";
echo "Доход с продаж: {$productReal->getTotal()} руб.<br>";
var_dump($productReal);


/*Цифровой товар*/
$productDigital = new \app\models\Product_Digital(137538198, "Комод", "Комод Sherlock",
    "Комод из коллекции Sherlock станет прекрасным приобретением для дома.",
    null, null, 969, null, "Россия", null);

echo "Покупка 1 (1 шт): {$productDigital->getPrice(1)} руб.<br>";
echo "Покупка 2 (2 шт): {$productDigital->getPrice(2)} руб.<br>";
echo "Доход с продаж: {$productDigital->getTotal()} руб.<br>";
var_dump($productDigital);


function sum($products)
{
    $sum = 0;
    foreach ($products as $product) {
        $sum += $product->getTotal();
    }
    return $sum;
}

echo "<hr>Доход со всех продаж составил: " . sum([$productWeight, $productReal, $productDigital]) . " руб.";